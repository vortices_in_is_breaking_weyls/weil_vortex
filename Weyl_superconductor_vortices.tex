\documentclass[aps,prb,twocolumn,amsmath,showpacs,amssymb,superscriptaddress]{revtex4}
%!TEX TS-program = ltex

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{dsfont}
\usepackage{color}
\newcommand{\nn}{\nonumber}
\newcommand{\pd}{{\phantom{\dagger}}}
\newcommand{\addblue}[1]{\textcolor{blue}{#1}}
\newcommand{\addred}[1]{\textcolor{red}{#1}}
\newcommand{\mpar}[1]{\marginpar{\small \it #1}}
\newcommand{\nobos}{{}^{\boldsymbol{\cdot}}_{\boldsymbol{\cdot}}}

\newcommand{\noteTM}[1]{{\color{magenta} [TM:\@#1]}}
\newcommand{\noteRI}[1]{{\color{blue} [RI:\@#1]}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\title{Title}

\author{authors}
\affiliation{affiliations}


\begin{abstract}
Abstract
\end{abstract}
\pacs{PACS}


\maketitle


\section{Introduction}

\section{A model Hamiltonian for an inversion-symmetry breaking superconducting Weyl system}
To study the physics of vortices in inversion symmetry breaking Weyl systems, we specialize to a simple model Hamiltonian describing the minimal number of four Weyl nodes confined to the $k_z$-axis in momentum space. The model is inspired by Ref.~\onlinecite{chen_16}, and physically describes a three-dimensional cubic lattice with two orbitals per unit cell, which we label by a pseudospin $\tau$. Introducing the spinor $\Psi^\dagger_{\mathbf{k}}=(c_{\mathbf{k},\uparrow,+1}^\dagger,c_{\mathbf{k},\downarrow,+1}^\dagger,c_{\mathbf{k},\uparrow,-1}^\dagger,c_{\mathbf{k},\downarrow,-1}^\dagger)$ of creation operators for electrons with momentum $\mathbf{k}$, spin $\sigma=\uparrow,\downarrow$, and orbital pseudospin $\tau=\pm1$, the normal-state Hamiltonian takes the form

\begin{align}
H_0&=\sum_{\mathbf{k}}\Psi_\mathbf{k}^\dagger\,\mathcal{H}_\mathbf{k}^0\,\Psi_\mathbf{k}^\pd,\label{eq:h0}\\
\mathcal{H}_\mathbf{k}^0&=\lambda\,\sin(k_x)\,\sigma_x\,\mathds{1}_{\tau}+\lambda\,\sin(k_y)\,\sigma_y\,\mathds{1}_{\tau}\nonumber\\
&+\lambda\,\sin(k_z)\,\left(\sigma_z
+b\,\mathds{1}_\sigma\right)\mathds{1}_\tau-\mu\,\mathds{1}_\sigma\,\mathds{1}_\tau\nonumber\\
&+\left[m+2-\cos(k_x)-\cos(k_y)\right]\left(\sigma_z+b\,\mathds{1}_\sigma\right)\,\tau_x,
\end{align}
where $\lambda$ corresponds to a hopping between sites in neighbouring unit cells, $m$ is a hopping between the two sites within a unit cell, and $\mu$ denotes the chemical potential. The  parameter $b$ finally governs a tilting of the Weyl nodes.

The normal state Hamiltonian $H_0$ has bands of energy $E_\pm^{\tilde{\tau}}=\pm\sqrt{\lambda^2\sin^2(k_x)+\lambda^2\sin^2(k_y)+M_{\tilde{\tau}}^2(\mathbf{k})}+b\,M_{\tilde{\tau}}(\mathbf{k})-\mu$ with $M_{\tilde{\tau}}(\mathbf{k})=\lambda\sin(k_z)+{\tilde{\tau}}(m+2-\cos(k_x)-\cos(k_y))$, where $\tilde{\tau}=\pm1$ denotes the orbital pseusospin eigenvalue. We from now on take $0<\lambda<1$, and focus on the regime $|m|<|\lambda|$ in which this Hamiltonian four Weyl nodes located at momentum $\mathbf{k} = \pm k_{z0} \hat{e}_z$ and  $\mathbf{k} = \pm(\pi- k_{z0}) \hat{e}_z$ with $k_{z0}=\arcsin(m/\lambda)$. While the model breaks inversion symmetry, it does respect the time-reversal symmetry $\mathcal{T}\,\mathcal{H}_\mathbf{k}\,\mathcal{T}^{-1} = \mathcal{H}_{-\mathbf{k}}$ with $\mathcal{T}=\mathcal{K}i\sigma_y\tau_z$ for $b=0$. The tilt parameter $b$ breaks this symmetry relating momenta $\mathbf{k}$ and $\mathbf{-k}$ in that it tilts the Weyl nodes at $\pm \mathbf{k}$ into the same direction. For $|b|>1$, the system becomes a type II Weyl semimetal. We disregard tilts of the Weyl nodes in opposite directions because they would not lead to the interesting physics associated with finite $b$, but comment on them below. The bulk spectrum of $H_0$ is illustrated in Fig~\ref{fig:normal_state_spectra} for generic sets of parameters.
 \begin{figure}
  \centering
  \includegraphics[width=0.75\columnwidth]{normal_state_spectrum_b0}  \includegraphics[width=0.75\columnwidth]{normal_state_spectrum_b05} \includegraphics[width=0.75\columnwidth]{normal_state_spectrum_b175}
  \caption{The spectrum of the normal-state Hamiltonian, see Eq.~\eqref{eq:h0}, as a function of $k_z$ (measured in units of the inverse lattice spacing) for $k_x=0=k_y$, $\lambda=0.75$, $m=0.5$, $\mu=0$, and $b=0$ (untilted nodes, top panel), $b=0.5$ (weakly tilted nodes, middle panel), $b=1.75$ (overtilted nodes, bottom panel).}
  \label{fig:normal_state_spectra}
\end{figure}

In the next step, we add superconductivity to the normal state Hamiltonian. Experimental evidence suggest that some Weyl systems are susceptible to intrinsic and induced superconductivity.\cite{pan_16,kang_15,qi_16,aggarwal_17,wang_tip_induced_17,bachmann_17,guguchia_17} {\color{red}more citations?} To date, however, the nature of the superconducting order(s) in these systems is under debate.\cite{lu_16} In our model, we assume superconductivity to be intrinsically present, and that the order parameter is of a simple BCS $s$-wave type pairing low-energy Weyl nodes. Since we are interested in the physics of vortices, which as we will discuss are governed by topological winding numbers and index theorems, we abstract away from the precise conditions under which this superconducting order forms for the given band structure of $H_0$, as well as the required form of the attractive interactions. Note, however, that this form of the order parameter is not only the simplest possible one, but that it could also allow the system to lower its energy further than competing nodal superconducting states because it leads to a full bulk gap. The Hamiltonian describing superconductivity with pairing amplitude $\Delta_\tau$ on the sublattice $\tau=\pm 1$ is given by

\begin{align}
H_{\rm SC}&=\sum_{\mathbf{k},\tau=\pm1}\Delta_\tau\,c_{\mathbf{k},\uparrow,\tau}^\dagger c_{-\mathbf{k},\downarrow,\tau}^\dagger+\rm{H.c.}.\label{eq:sc_0}
\end{align}
From the diagonalization of the $\tau$-sector of $\mathcal{H}_{\mathbf{k}}^0$, we see that the Weyl nodes at momentum $\mathbf{k}$ and $-\mathbf{k}$ belong to different eigenvalues $\tilde{\tau}$. The pairing in Eq.~\eqref{eq:sc_0} can thus only open up a gap if its amplitude differs on the two sublattices, $\Delta_{+1}\neq\Delta_{-1}$, which is generically allowed. Projecting the pairing onto the coupling of states close to the Weyl nodes, we obtain

\begin{align}
H_{\rm SC}&\approx\sum_{\mathbf{k},\tilde{\tau}=\pm1}\Delta\,c_{\mathbf{k},\uparrow,\tilde{\tau}}^\dagger c_{-\mathbf{k},\downarrow,-\tilde{\tau}}^\dagger+\rm{H.c.}\label{eq:sc_1}
\end{align}
with $\Delta = (\Delta_2-\Delta_1)/2$. In the eigenbasis of the $\tau$-sector, we can thus write the full Hamiltionan as

\begin{align}
H&=H_0+H_{\rm SC} \approx \sum_{\mathbf{k}}\tilde{\Psi}_{\mathbf{k}}^\dagger\, \mathcal{H}_{\mathbf{k}}\,\tilde{\Psi}_{\mathbf{k}},\\
\mathcal{H}_{\mathbf{k}}&=\begin{pmatrix}\mathcal{H}_{\mathbf{k}}^{+}&\Delta \, i\sigma_y\\-\Delta \, i\sigma_y&-\mathcal{H}_{-\mathbf{k}}^{-}{}^T\end{pmatrix},
\end{align}
where
\begin{align}
\mathcal{H}_{\mathbf{k}}^{\tilde{\tau}} &=\lambda\,\sin(k_x)\,\sigma_x+\lambda\,\sin(k_y)\,\sigma_y\nonumber\\
&+\lambda\,\sin(k_z)\,\left(\sigma_z
+b\,\mathds{1}_\sigma\right)-\mu\,\mathds{1}_\sigma\nonumber\\
&+\tilde{\tau}\left[m+2-\cos(k_x)-\cos(k_y)\right]\left(\sigma_z+b\,\mathds{1}_\sigma\right),\\
\tilde{\Psi}_{\mathbf{k}}^\dagger&=(c_{\mathbf{k},\uparrow,\tilde{\tau}=+1}^\dagger,c_{\mathbf{k},\downarrow,\tilde{\tau}=+1}^\dagger,c_{-\mathbf{k},\uparrow,\tilde{\tau}=-1}^\dagger,c_{-\mathbf{k},\downarrow,\tilde{\tau}=-1}^\dagger).
\end{align}
Because the relevant physics derives from the  low-energy modes close to the Weyl nodes at $k_x=0=k_y$, we can further simplify the Hamiltonian without losing track of its topology by expanding $H$ to linear order in $k_x$ and $k_y$. After an additional basis change to the spinor $\Phi_{\mathbf{k}}^\dagger=(c_{\mathbf{k},\uparrow,\tilde{\tau}=+1}^\dagger,c_{\mathbf{k},\downarrow,\tilde{\tau}=+1}^\dagger,-c_{-\mathbf{k},\downarrow,\tilde{\tau}=-1}^\dagger,c_{-\mathbf{k},\uparrow,\tilde{\tau}=-1}^\dagger)$, we obtain $H\approx \sum_{\mathbf{k}}\Phi_{\mathbf{k}}^\dagger\, \tilde{\mathcal{H}}_{\mathbf{k}}\,\Phi_{\mathbf{k}}$ with 
\begin{widetext}
\begin{align}
\tilde{\mathcal{H}}_{\mathbf{k}}&=\begin{pmatrix}
\epsilon_+(k_z)-\mu&\lambda(k_x-ik_y)&\Delta&0\\\lambda(k_x+ik_y)&-\epsilon_-(k_z)-\mu&0&\Delta
\\\Delta^*&0&-\epsilon_-(k_z)+\mu&-\lambda(k_x-ik_y)\\0&\Delta^*&-\lambda(k_x+ik_y)&\epsilon_+(k_z)+\mu
\end{pmatrix}
\end{align}
\end{widetext}
with $\epsilon_{\pm}(k_z)=(1\pm b)(\lambda\,\sin(k_z)+m)$. For finite $\Delta$, and not too strong tilts, $H$ describes a fully gapped superconductor.\cite{hosur_14,chen_16} {\color{red}when exactly can we get a topological superconductor with surface states as mentioned by reference \onlinecite{hosur_14}? Is the gap of opposite sign on the two Fermi surfaces all we need?}

\section{The index theorem, and vortex bound states}
In the presence of a sufficiently large magnetic field, superconductors of type II allow magnetic flux tubes to penetrate their bulk. Because the superconducting order parameter acquires a phase proportional to the flux trapped inside the vortex, the single-valuedness of the order parameter implies that the flux in a vortex is quantized in units of the superconducting flux quantum $\Phi_0=\pi/e$ (in units of $\hbar=1$). In the following, we study the physics of states bound to these vortices. Because the Weyl nodes are aligned with the $k_z$-axis in momentum space, we from now on spezialize to magnetic fields applied along the ${z}$-axis. We neglect the Zemann effect, and approximate the magnetic field to be constant inside the vortex core (which is a cylinder of radius $R$), and to vanish outside the core. The vortex suppressed the superconducting order parameter inside the vortex, which gives rise to a radially varying gap amplitude $|\Delta(r)|$, with $|\Delta(0)|=0$ and $|\Delta(r\to\infty)|=\Delta_0$. Neglecting the Zeeman effect, a single vortex trapping $m$ flux quanta is described by the Hamiltonian
\begin{widetext}
\begin{align}
H&\approx \int dr\,\int r d\theta \,\sum_{k_z}\Phi_{k_z}^\dagger(r,\theta)\,\tilde{\mathcal{H}}_{k_z}(r,\theta)\,\Phi_{k_z}^\pd(r,\theta)\\
\tilde{\mathcal{H}}_{k_z}(r,\theta)&=\begin{pmatrix}
\epsilon_+(k_z)-\mu&-i\lambda\,e^{-i\theta}(\partial_r-i\frac{\partial_\theta-i\mathcal{A}(r)}{r})&\Delta(r,\theta)&0\\-i\lambda\,e^{i\theta}(\partial_r+i\frac{\partial_\theta-i\mathcal{A}(r)}{r})&-\epsilon_-(k_z)-\mu&0&\Delta(r,\theta)
\\\Delta^*(r,\theta)&0&-\epsilon_-(k_z)+\mu&i\lambda\,e^{-i\theta}(\partial_r-i\frac{\partial_\theta+i\mathcal{A}(r)}{r})\\0&\Delta^*(r,\theta)&i\lambda\,e^{i\theta}(\partial_r+i\frac{\partial_\theta+i\mathcal{A}(r)}{r})&\epsilon_+(k_z)+\mu
\end{pmatrix}
\end{align}
\end{widetext}
with
\begin{align}
\Delta(r,\theta)&=|\Delta(r)|\,e^{i\varphi}\,e^{im\theta},\\
\mathcal{A}(r)&= \frac{m}{2}\left(\frac{r^2}{R^2}\Theta(R-r)+\Theta(r-R)\right).\label{eq:fullA}
\end{align}

\subsection{Index theorem and number of bound state modes}\label{sec:index_theorem}
The number of bound state modes of this Hamiltonian can be deduced from an index theorem\cite{weinberg_81,jackiw_81,fuji_11,schuster_16} relating the topologically protected number of zero modes associated with a vortex in a Weyl system to the winding number of the vortex. %The index theorem is based on the linear expansion of the spectrum close to each Weyl node. %We explicate the calculation for the node at momentum $k_{z0}$, close to which we can expand $\epsilon_\pm(k_z+q_z)\approx (1\pm b)\,v_F\,q_z$ with $v_F$ denoting the Fermi velocity. It is now helpful to introduce the matrices
The discussion of the theorem is simplified by the introduction of the matrices

\begin{align}
\Gamma^{1,2,3}&=\sigma_{x,y,z}\otimes\sigma_z,\\
\Gamma^4&=\mathds{1}\otimes\sigma_x,\\
\Gamma^5&=\mathds{1}\otimes\sigma_y,
\end{align}
which satisfy the anticommutator relation
\begin{align}
\left\{\Gamma^a,\Gamma^b\right\}=2\delta_{a,b}\,\mathds{1}_{4\times4}.
\end{align}
In terms of these matrices, the Hamiltonian takes the form
\begin{align}
\tilde{\mathcal{H}}_{k_{z}}(r,\theta)=&\tilde{\mathcal{H}}_{\perp}(r,\theta) + \tilde{\mathcal{H}}_z^3(k_z)+\tilde{\mathcal{H}}_z^0(k_z)+\tilde{\mathcal{H}}_\mu
\end{align}
with
\begin{align}
\tilde{\mathcal{H}}_{\perp}(r,\theta)=&-i\lambda \,\left(\cos(\theta)\partial_r-\sin(\theta)\frac{\partial_\theta-i\mathcal{A}(r)}{r}\right)\Gamma^1\nonumber\\
&-i\lambda \,\left(\sin(\theta)\partial_r+\cos(\theta)\frac{\partial_\theta-i\mathcal{A}(r)}{r}\right)\Gamma^2\nonumber\\
&+\text{Re}\left\{\Delta(r,\theta)\right\}\,\Gamma^4-\text{Im}\left\{\Delta(r,\theta)\right\}\,\Gamma^5,\\
\tilde{\mathcal{H}}_{z}^3(k_z)=&(\lambda \sin(k_z)+m)\,\Gamma^3,\\
\tilde{\mathcal{H}}_{z}^0(k_z)=&b\,(\lambda \sin(k_z)+m)\,\mathds{1}_{\rm 4\times4},\\
\tilde{\mathcal{H}}_\mu=&i\mu\,\Gamma^4\Gamma^5.
\end{align}
Let us first focus on $\tilde{\mathcal{H}}_{\perp}$. This matrix has the chiral symmetry $\Gamma^3 \, \tilde{\mathcal{H}}_{\perp}\,\Gamma^3 =-\tilde{\mathcal{H}}_{\perp}$, which implies that for every eigenstate $\Psi$ with eigenvalue $E$, there is an eigenstate $\Psi'=\Gamma^3\,\Psi$ with eigenvalue $-E$. The matrix $\tilde{\mathcal{H}}_{z}^3$ couples these two eigenstates $\Psi$ and $\Psi'$. As a result, the eigenstates of $\tilde{\mathcal{H}}_{\perp}+\tilde{\mathcal{H}}_{z}^3$ have eigenvalues $\tilde{E}(k_z)=\pm \sqrt{E^2+(\lambda \sin(k_z)+m)^2}$, and are gapped unless $E=0$. For $E=0$, on the other hand, the eigenstates of $\tilde{\mathcal{H}}_{\perp}+\tilde{\mathcal{H}}_{z}^3$ are also eigenstates of $\Gamma^3$. Their eigenvalues with respect to $\tilde{\mathcal{H}}_{\perp}+\tilde{\mathcal{H}}_{z}^3$ are given by $\tilde{E}=\gamma^3 (\lambda \sin(k_z)+m)$, and vanish at the momentum $k_z$ of the Weyl nodes (where $\gamma^3=\pm1$ denotes the respective $\Gamma^3$-eigenvalue).\footnote{Note that the nodes at positive $k_z$ have been mapped to negative $k_z$ by the introduction of the Nambu spinors.} 

The number of eigenstates of $\tilde{\mathcal{H}}_{\perp}+\tilde{\mathcal{H}}_{z}^3$ whose dispersion crosses zero at the Weyl node momenta is thus equal to the number of zero modes of $\tilde{\mathcal{H}}_{\perp}$. 
However, not all of these zero modes are protected: additional perturbations to the Hamiltonian can in principle gap out some or all of these zero-modes. In the present case, there are two classes of perturbations that lead to different numbers of zero modes. Perturbations $H_{\rm pert}$ that anticommute with $\Gamma^3$ only couple zero modes with different $\Gamma^3$-eigenvalues, which follows from
\begin{align}
\langle \Psi|H_{\rm pert}|\tilde{\Psi}\rangle &= \gamma^3\tilde{\gamma}^3\langle \Psi|\Gamma^3H_{\rm pert}\Gamma^3|\tilde{\Psi}\rangle \nonumber\\
&= -\gamma^3 \tilde{\gamma}^3\,\langle \Psi|H_{\rm pert}|\tilde{\Psi}\rangle.
\end{align}
Because the direction of motion of one of these modes at momentum $k_z$ is given by the sign of the group velocity $\partial_{k_z} \tilde{E}(k_z)$, and hence corresponds to its $\Gamma^3$-eigenvalue, this class of perturbations couples counterpropagating modes. This in turn gaps them at the Weyl node momenta, where their dispersions cross. The number of topologically protected states in a system with chiral symmetryis thus given by the number of modes with positive $\Gamma^3$-eigenvalue, minus the number of modes with negative $\Gamma^3$-eigenvalue,
\begin{align}
\mathcal{N}_0(\tilde{\mathcal{H}}_{\perp}+\tilde{\mathcal{H}}_{z}^3)%&= \sum_{\text{zero~modes~of~}\mathcal{H}_{\perp}} \Gamma^3\text{-eigenvalue~of~mode}\nonumber\\
=\sum_{\text{all~states~}j}\lim_{\epsilon\to0}\,\langle j | \Gamma^3\,\frac{\epsilon^2}{\tilde{\mathcal{H}}_{\perp}^2+\epsilon^2}|j\rangle
\end{align}
where $\lim_{\epsilon\to0}\,\epsilon^2\,(\tilde{\mathcal{H}}_{\perp}^2+\epsilon^2)^{-1}$ acts as a projector onto the zero energy states of $\tilde{\mathcal{H}}_{\perp}$. As is detailed in Ref.~\onlinecite{fuji_11}, one can show that this number is precisely given by the vorticity, $\mathcal{N}_0(\tilde{\mathcal{H}}_{\perp}+\tilde{\mathcal{H}}_{z}^3)=m$. The number $\mathcal{N}_0(\tilde{\mathcal{H}})$ is also called the index of the matrix $\tilde{\mathcal{H}}$.

In contrast, perturbations that do not anticommute with $\Gamma^3$ generically also couple modes with the same $\Gamma^3$-eigenvalue. Because $\tilde{\mathcal{H}}_\mu$ commutes with $\Gamma^3$, the chemical potential is an example for such a perturbation. Since modes with the same $\Gamma^3$-eigenvalue have the same direction of propagation, these perturbations do not directly open up a gap, but generically shift the dispersions to larger or smaller energies. For sufficiently large perturbations, this will push entire dispersion branches above or below the chemical potential.

As long as the Hamiltonian still has a particle-hole symmetry, however, this can only happen for pairs of bands being pushed into opposite directions. If there is an odd number of bound state branches, this implies that there remains a topologically protected zero-energy crossing for one of the bound state branches. What is more, the particle-hole symmetry does in fact not have to be obeyed in the entire Brillouin zone, but only at the momenta of the Weyl nodes. This is precisely the case for the full Hamiltonian $\tilde{\mathcal{H}}_{k_z}(r,\theta)$. Close to the Weyl node momenta, we can expand the Hamiltonian to linear order in the momentum difference to the Weyl nodes. As an example, consider $k_z=-k_{z0}+q_z$ with $|q_z|\ll k_{z0}$. Linearizing the Hamiltonian amounts to replacing $\lambda(\sin(k_z)+m)\to\lambda\cos(k_{z0})\,q_z$. In this linear approximation valid close to the Weyl nodes, but not in its full form describing the entire Brillouin zone, the Hamiltonian has the particle hole symmetry $\mathcal{C}\,\tilde{\mathcal{H}}_{k_{z0}+q_z}(r,\theta)\,\mathcal{C}^{-1}=-\tilde{\mathcal{H}}_{k_{z0}-q_z}(r,\theta)$ with $\mathcal{C}=\mathcal{K}\Gamma^1\Gamma^4$.  Now applying the argument of pairwise splitting off modes from zero energy to momentum $k_{z0}$, we find that this restricted particle-hole symmetry is sufficient to pin one bound state mode to zero energy at momentum $k_{z0}$. We can thus classify the number of vortex bound state branches that have topologically protected zero energy crossings by a $\mathds{Z}_2$-index: if the vortex has an odd vorticity, there is one topologically protected bound state mode crossing zero energy, while there is no topologically protected zero-energy crossing for an even vorticity. 

\subsection{Vortex bound states}
Because the system is rotationally invariant around the vortex axis, the eigenstates of the full Hamiltonian can be classified by $k_z$, the angular momentum $l_z$, and a radial quantum number $n$. To solve the Schr\"odinger equation $H\,|n,l_z,k_z\rangle = E_{n,l_z}(k_z)\,|n,l_z,k_z\rangle$, we use the Ansatz

\begin{align}
|n,l_z,k_z\rangle = \int dr\int r d\theta \,\sum_{j=1}^4\,g_{j,l_z,k_z}^{(n)}\,\Phi_{k_z,j}^\dagger(r,\theta)\,|0\rangle
\end{align}
where $\Phi_{k_z,j}^\dagger(r,\theta)$ is the $j$-th component of $\Phi_{k_z}^\dagger(r,\theta)$, while $|0\rangle$ denotes the vacuum that is annihilated by all components of $\Phi_{k_z}^\pd(r,\theta)$. With this Ansatz, the Schr\"odinger equation can be rewritten as a differential equation for the coefficients $g_{j,l_z,k_z}^{(n)}$,

\begin{align}
\tilde{\mathcal{H}}_{k_z}(r,\theta)\,\begin{pmatrix}g_{1,l_z,k_z}^{(n)}(r,\theta)\\g_{2,l_z,k_z}^{(n)}(r,\theta)\\g_{3,l_z,k_z}^{(n)}(r,\theta)\\g_{4l_z,,k_z}^{(n)}(r,\theta)\end{pmatrix} = E_{n,l_z}(k_z) \,\begin{pmatrix}g_{1,l_z,k_z}^{(n)}(r,\theta)\\g_{2,l_z,k_z}^{(n)}(r,\theta)\\g_{3,l_z,k_z}^{(n)}(r,\theta)\\g_{4,l_z,k_z}^{(n)}(r,\theta)\end{pmatrix}\label{eq:full_sg}
\end{align}
The angular momentum $l_z$ is formally the eigenvalue of an eigenstate with respect to the operator
\begin{align}
{\mathcal{L}}_z&=\begin{pmatrix}-i\partial_\theta&0&0&0\\0&-i\partial_\theta-1&0&0\\0&0&-i\partial_\theta+m&0\\0&0&0&-i\partial_\theta+m-1\end{pmatrix},
\end{align}
which commutes with $\tilde{\mathcal{H}}_{k_z}(r,\theta)$. This motivates us to further simplify the Ansatz as

\begin{align}
\begin{pmatrix}g_{1,l_z,k_z}^{(n)}(r,\theta)\\g_{2,l_z,k_z}^{(n)}(r,\theta)\\g_{3,l_z,k_z}^{(n)}(r,\theta)\\g_{4,l_z,k_z}^{(n)}(r,\theta)\end{pmatrix} = \begin{pmatrix}e^{i\varphi/2}e^{i l_z\theta}\,{f}_{1,k_z}^{(n)}(r)\\e^{i\varphi/2}e^{i (l_z+1)\theta}\,{f}_{2,k_z}^{(n)}(r)\\-ie^{-i\varphi/2}e^{i (l_z-m)\theta}\,{f}_{3,k_z}^{(n)}(r)\\-ie^{-i\varphi/2}e^{i (l_z-m+1)\theta}\,{f}_{4,k_z}^{(n)}(r)\end{pmatrix} \label{eq:refined_ansatz}
\end{align}
which in turn leads to
\begin{align}
\tilde{\mathcal{H}}_{l_z,k_z}^{({f})}(r)\,\begin{pmatrix}{f}_{1,k_z}^{(n)}(r)\\{f}_{2,k_z}^{(n)}(r)\\{f}_{3,k_z}^{(n)}(r)\\{f}_{4,k_z}^{(n)}(r)\end{pmatrix} =0\label{eq:bs_ev_eq_is}
\end{align}
with
\begin{widetext}
\begin{align}
\tilde{\mathcal{H}}_{l_z,k_z}^{({f})}(r,\theta)&=\begin{pmatrix}
\epsilon_+(k_z)-\mu-E_{n,l_z}(k_z)&-i\lambda\,(\partial_r+\frac{l_z+1-\mathcal{A}(r)}{r})&-i|\Delta(r)|&0\\-i\lambda\,(\partial_r-\frac{l_z-\mathcal{A}(r)}{r})&-\epsilon_-(k_z)-\mu-E_{n,l_z}(k_z)&0&-i|\Delta(r)|
\\i|\Delta(r)|&0&-\epsilon_-(k_z)+\mu-E_{n,l_z}(k_z)&i\lambda\,(\partial_r+\frac{l_z-m+1+\mathcal{A}(r)}{r})\\0&i|\Delta(r)|&i\lambda\,(\partial_r-\frac{l_z-m+\mathcal{A}(r)}{r})&\epsilon_+(k_z)+\mu-E_{n,l_z}(k_z)
\end{pmatrix}.
\end{align}
\end{widetext}
The details of the explicit solution of this equation can be found in Appendix \ref{append:bound_state_modes}. For $\mu=0$, we find that there are two classes of solutions. For positive values of the magnetic flux, $m>0$, we find that there exist indeed precisely $m$ normalizable bound state solutions, which have angular momentum $0\leq l_z\leq m-1$. All of these solutions have ${f}_{2,k_z}^{(n)}(r)=0={f}_{3,k_z}^{(n)}(r)$, and are degenerate with energy $E_{n,l_z}(k_z) = \epsilon_+(k_z)=(1+ b)(\lambda\,\sin(k_z)+m)$. For negative values of the magnetic flux, $m<0$, there are again $m$ normalizable bound states, whose angular momentum is $m\leq l_z\leq -1$. All of these solutions have ${f}_{1,k_z}^{(n)}(r)=0={f}_{4,k_z}^{(n)}(r)$, and are degenerate with energy $E_{n,l_z}(k_z) = -\epsilon_-(k_z)=-(1- b)(\lambda\,\sin(k_z)+m)$. In agreement with the index theorem discussed in Sec.~\ref{sec:index_theorem}, we thus find that for any finite $m$, a vortex binds $m$ degenerate bound state modes that have a sinusodial dispersion with $k_z$, the momentum along the vortex. As long as the corresponding normal state system has Weyl nodes, the dispersion of these $m$ bound state modes crosses zero energy at the Weyl node momenta $k_z$. If the system is time-reversal symmetric, $b=0$, the dispersions are simply inverted upon reversing the magnetic field. In particular, the velocities of the bound state modes at Weyl node momenta remains identical for either field direction. In the time-reversal symmetry broken case, the dispersions of the bound states differ when the magnetic field is reversed, giving rise to a ratio of velocities $v_{m>0}/v_{m<0} = (1+b)/(1-b)$. For $\mu=0$ and odd $m$, finally, we that the single normalizable bound state predicted by the index theorem has angular momentum $l_z = ({m-1})/{2}$, and crosses the Fermi level as long as the normal state has Weyl nodes.

\section{Discussion}
\begin{itemize}
\item Weyl nodes present: gapless modes. Else not. Can be seen in thermal transport
\item With disorder: Luttingwer liquid powerlaws. If TRS is broken, the powerlaws are different
\item Thus, thermal transoprt in vortices can unveil underlying Weyl character.
\end{itemize}
{\color{red}Figure with dispersion}
\section{Summary and conclusions.}

\begin{acknowledgments}
This work was funded by $\ldots$ (R.I.), and the Deutsche Forschungsgemeinschaft through SFB 1143, as well as the Emmy-Noether program ME 4844/1 (T.M.).
\end{acknowledgments}


\appendix

\section{Explicit solution of the bound state modes for $\mu=0$}\label{append:bound_state_modes}
To illustrate the solution of Eq.~\eqref{eq:bs_ev_eq_is}, we restrict ourselves to the case $\mu=0$ and show that that for $m>0$, one can construct precisely $m$ bound states that have $E_{n,l_z}(k_z) = \epsilon_+(k_z)$,  ${f}_{2,k_z}^{(n)}(r)=0$, and ${f}_{3,k_z}^{(n)}(r)=0$. In a similar fashion, one can show that for $m<0$, there are $m$ bound states with $E_{n,l_z}(k_z) = \epsilon_-(k_z)$,  ${f}_{1,k_z}^{(n)}(r)=0$, and ${f}_{4,k_z}^{(n)}(r)=0$. These are precisely the bound states whose existence is guaranteed by the index theorem, which is discussed in Sec.~\ref{sec:index_theorem}. With these assumptions, the Schr\"odinger equation can be reduced to
%\begin{widetext}
\begin{align}
&\mathcal{H}_{\epsilon_+}\begin{pmatrix}{f}_{1,k_z}^{(n)}(r)\\{f}_{4,k_z}^{(n)}(r)\end{pmatrix} =0\label{eq:append_full_sg}
\end{align}
with
\begin{align}
\mathcal{H}_{\epsilon_+}&=\begin{pmatrix}-i\lambda\,(\partial_r-\frac{l_z-\mathcal{A}(r)}{r})&-i|\Delta(r)|
\\i|\Delta(r)|&i\lambda\,(\partial_r+\frac{l_z-m+1+\mathcal{A}(r)}{r})
\end{pmatrix}.
\end{align}
%\end{widetext}

\subsection{Solution far outside the vortex}
Far outside the vortex, the amplitude of the superconducting order parameter approaches a finite constant, $|\Delta(r)|\to\Delta_0$. With Eq.~\eqref{eq:fullA}, we then find
\begin{align}
\mathcal{H}_{\epsilon_+}&=\begin{pmatrix}-i\lambda\,(\partial_r-\frac{l_z-m/2}{r})&-i\,\Delta_0
\\i \,\Delta_0&i\lambda\,(\partial_r+\frac{l_z-m/2+1}{r})\label{eq:append_start}
\end{pmatrix}
\end{align}
From Eq.~\eqref{eq:append_start}, we obtain
\begin{align}
\left(r^2\partial_r^2 +r \partial_r-\left[r^2\frac{\Delta_0^2}{\lambda^2}+\left(l_z-\frac{m}{2}\right)^2\right]\right)\,{f}_{1,k_z}^{(n)}(r)&=0,\\
\left(r^2\partial_r^2 +r \partial_r-\left[r^2\frac{\Delta_0^2}{\lambda^2}+\left(l_z+1-\frac{m}{2}\right)^2\right]\right)\,{f}_{4,k_z}^{(n)}(r)&=0.
\end{align}
The solutions to these differential equations are modified Bessel functions. Because the eigenstates need to be normalizable, the exponentially growing modified Bessel functions of the first kind need to be excluded, which leads us to the solution
\begin{align}
{f}_{1,k_z}^{(n)}(r) = f_{1,k_z,0}^{(n)}\,K_{|l_z-m/2|}\left(\frac{\Delta_0}{|\lambda|}r\right),\\
{f}_{4,k_z}^{(n)}(r) = f_{4,k_z,0}^{(n)}\,K_{|l_z+1-m/2|}\left(\frac{\Delta_0}{|\lambda|}r\right).
\end{align}
Because $K_\nu(r)\to \sqrt{\frac{\pi}{2x}}\,e^{-x}$ for $x\gg1$, we furthermore find that Eq.~\eqref{eq:append_start} can only be fulfilled for $r\to\infty$ if $f_{1,k_z,0}^{(n)}=f_{4,k_z,0}^{(n)}$.

\subsection{Solution deep inside the vortex}
Deep inside the vortex, we specialize to most simple case $|\Delta(r)|=0$ for sufficiently small $r/R$, where $R$ is the vortex core radius (but the calculation can easily be extended to the $|\Delta(r)|\to\delta\,r^\gamma$ with small $\delta$ and $\gamma \geq0$). We then find

\begin{align}
\left(\partial_r-\frac{l_z}{r}+ \frac{m}{2}\frac{r}{R^2}\right)\,{f}_{1,k_z}^{(n)}(r)&=0,\\
\left(\partial_r-\frac{m-l_z-1}{r}+ \frac{m}{2}\frac{r}{R^2}\right)\,{f}_{4,k_z}^{(n)}(r)&=0.
\end{align}
For $r\to 0$, the vector-potential terms $mr/2R^2$ are negligibly small, and we obtain
\begin{align}
{f}_{1,k_z}^{(n)}(r)&\approx \tilde{f}_{1,k_z,0}^{(n)}\,r^{l_z},\label{eq:append_sol_1}\\
{f}_{4,k_z}^{(n)}(r)&\approx \tilde{f}_{4,k_z,0}^{(n)}\,r^{m-l_z-1}.\label{eq:append_sol_4}
\end{align}
Both of these solutions need to be normalizable, which is the case if the integrals $\int d^r  |{f}_{i,k_z}^{(n)}(r)|^2$ are convergent at the origin. We thus find that ${f}_{1,k_z}^{(n)}(r)$ is normalizable if $2 l_z+1 > -1$, and hence for $l_z > -1$. The function ${f}_{4,k_z}^{(n)}(r)$ is normalizable at the origin if $2 (m-l_z-1)+1 > -1$, which is the case for $l_z <m$. Because $l_z$ and $m$ are integers, we thus find that a normalizable bound state solution is only possible for $0\leq l_z\leq m-1$.

\subsection{Matching of the solutions at the vortex core boundary, and the determination of their energy}
To find the full solution, we use Eq.~\eqref{eq:append_full_sg} to propagate the solutions found for large and  small $r$ to the vortex core boundary. At this interface, we need to match both ${f}_{1,k_z}^{(n)}(r)$ and ${f}_{4,k_z}^{(n)}(r)$, which yields two conditions. Another condition is given by the global normalization of the wave function, which fully determines the integration constants $\tilde{f}_{1,k_z,0}^{(n)}$, $\tilde{f}_{4,k_z,0}^{(n)}$, and ${f}_{1,k_z,0}^{(n)}={f}_{4,k_z,0}^{(n)}$ .

To discuss the matching of the wave functions more generally, and to understand how their energies can be determined, consider starting from the full Schr\"odinger equation, in which the energy $E_{n,l_z}(k_z)$ is at first unknown. Because Eq.~\eqref{eq:full_sg} is a $(4\times4)$-matrix equation, it has four linearly independent solutions for any choice of $E_{n,l_z}(k_z)$. The form of these solutions depends on this choice, but we find for any $E_{n,l_z}(k_z)$ that only two of them are normalizable for $r\to\infty$. In the region inside the vortex, we also find two linearly independent normalizable solutions for any given value of the energy. At the interface, we need to match all four components ${f}_{i,k_z}^{(n)}(r)$ of the wave function, which gives four conditions (note that because the present problem's Schr\"odinger equation is a differential equation of first order, the matching of the wave function itself ensures the matching of the derivatives). An additional condition arises from the global normalization of the wave function. To match the inside and outside solutions in a normalized way, we thus need five unknowns to be solved for, which precisely are the prefactors of the linearly independent solutions inside and outside, and the energy. 
%%%%%%%%%%%%%%%%%%%%%%%

\begin{thebibliography}{99}
\bibitem{chen_16}
A. Chen and M. Franz, Phys. Rev. B \textbf{93}, 201105(R) (2016).

\bibitem{pan_16}
X.-C. Pan, X. Chen, H. Liu, Y. Feng, Z. Wei, Y. Zhou, Z. Chi, L. Pi, F. Yen, F. Song, X. Wan, Z. Yang, B. Wang, G. Wang, Y. Zhang, Nature Commun. \textbf{6}, 7805 (2015).

\bibitem{kang_15}
D. Kang, Y. Zhou, W. Yi, C. Yang, J. Guo, Y. Shi, S. Zhang, Z. Wang, C. Zhang, S. Jiang, A. Li, K. Yang, Q. Wu, G. Zhang, L. Sun, and Z. Zhao, Nature Commun. \textbf{6}, 7804 (2015).


\bibitem{qi_16}
Y. Qi, P. G. Naumov, M. N. Ali, C. R. Rajamathi, W. Schnelle, O. Barkalov, M. Hanfland, S.-C. Wu, C. Shekhar, Y. Sun, V. S\"u\ss, M. Schmidt, U. Schwarz, E. Pippel, P. Werner, R. Hillebrand, T. F\"orster, E. Kampert, S. Parkin, R.J. Cava, C. Felser, B. Yan, and S. A. Medvedev, Nature Commun. \textbf{7}, 11038 (2016).

\bibitem{bachmann_17}
M. D. Bachmann, N. Nair, F. Flicker, R. Ilan, T. Meng, N. J. Ghimire, E. D. Bauer, F. Ronning, J. G. Analytis, P. J.W. Moll, arXiv:1703.08024.

\bibitem{aggarwal_17}
L. Aggarwal, S. Gayen, S. Das, R. Kumar, V. S\"u\ss, C. Felser, C. Shekhar, and G. Sheet, Nature Commun. \textbf{8}, 13974 (2017).

\bibitem{wang_tip_induced_17}
H. Wang, H. Wang, Y. Chen, J. Luo, Z. Yuan, J. Liu, Y. Wang, S. Jia, X.-J. Liu, J. Wei, J. Wang, Science Bulletin \textbf{62}, 425 (2017).

\bibitem{guguchia_17}
Z. Guguchia, F. v. Rohr, Z. Shermadini, A. T. Lee, S. Banerjee, A. R. Wieteska, C. A. Marianetti, H. Luetkens, Z. Gong, B. A. Frandsen, S. C. Cheung, C. Baines, A. Shengelaya, A. N. Pasupathy, E. Morenzoni, S. J. L. Billinge, A. Amato, R. J. Cava, R. Khasanov, Y. J. Uemura, arXiv:1704.05185.

\bibitem{lu_16}
 P. Lu, J.S. Kim, J. Yang, H. Gao, J. Wu, D. Shao, B. Li, D. Zhou, J. Sun, D. Akinwande, D. Xing, and J.-F. Lin, Phys. Rev. B \textbf{94}, 224512 (2016).


\bibitem{hosur_14}
P. Hosur, X. Dai, Z. Fang, and X.-L. Qi, Phys. Rev. B \textbf{90}, 045130 (2014).



\bibitem{weinberg_81}
E. J. Weinberg, Phys. Rev. D \textbf{24}, 2669 (1981).

\bibitem{jackiw_81}
R. Jackiw and P. Rossi, Nucl. Phys. \textbf{B190}, 681 (1981).

\bibitem{fuji_11}
T. Fujiwara, T. Fukui, M. Nitta, and S. Yasui,
Phys. Rev. D \textbf{84}, 076002 (2011).

\bibitem{schuster_16}
T.~Schuster, T.~Iadecola, C.~Chamon, R.~Jackiw, and S.-Y.~Pi, Phys. Rev. B \textbf{94}, 115110 (2016).

%%%

\bibitem{de_juan}
F. de Juan, A. Cortijo, and M A. H. Vozmediano, Phys. Rev. B \textbf{76}, 165409 (2007).

\bibitem{zhang_vish}
Y. Zhang and A. Vishwanath, Phys. Rev. Lett. \textbf{105}, 206601 (2010).

\bibitem{jellal_9}
A. Jellal, A. D. Alhaidari, and H. Bahlouli, Phys. Rev. A \textbf{80}, 012109 (2009).

\bibitem{meng_balents_12}
T. Meng and L. Balents, Phys. Rev B \textbf{86}, 054504 (2012)  \textit{[note that there is a mistake in this calculation which does not affect the qualitative statements made in these notes]}.

\bibitem{chiu_11}
C.-K. Chiu, M. J. Gilbert, and T. L. Hughes, Phys. Rev. B \textbf{84},144507 (2011).
\bibitem{hosur_11}
P. Hosur, P. Ghaemi, R. S. K. Mong, and A. Vishwanath, Phys. Rev. Lett. 107, 097001 (2012).
\bibitem{hou_7}
C.-Y. Hou, C. Chamon, and C. Mudry, Phys. Rev. Lett. \textbf{98}, 186809 (2007).

\bibitem{fukui_10}
T. Fukui, and T. Fujiwara, J. Phys. Soc. Jpn. \textbf{79}, 033701 (2010).


\bibitem{giamarchi_book}
T. Giamarchi, {\it Quantum Physics in One Dimension} (Oxford University Press, 2003).

\bibitem{origniac}
M.-R. Li and E. Orignac, Europhys. Lett. \textbf{60}, 432 (2002).




\end{thebibliography}
\end{document}

